﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas
{
    class ServerIDs
    {

        public static ulong AtlasID()
        {
            return 227778876540059651;
        }

        public static ulong TestID()
        {
            return 285460149769076737;
        }

        public static ulong StaffID()
        {
            return 291643233682063370;
        }
    }
}
