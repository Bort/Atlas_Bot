﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RiotSharp;
using Discord;
using Discord.Commands;

namespace Atlas
{
    public partial class Form1 : Form
    {
        DiscordClient BotUser;
        CommandService commands;
        private RiotApiHandler Rito = new RiotApiHandler();
        private DatabaseHandler SQL = new DatabaseHandler();
        public Form1()
        {
            InitializeComponent();
            BotUser = new DiscordClient(x =>
            {
                x.LogLevel = LogSeverity.Info;

            });
             BotUser.Log.Message += (s, e) => Console.WriteLine($"[{e.Severity}] {e.Source}: {e.Message}");

            BotUser.UsingCommands(x =>
             {
                 x.PrefixChar = '-';
                 x.AllowMentionPrefix = true;

             });
            commands = BotUser.GetService<CommandService>();
            //PutCommandsHere
            Help();
            //Retard Role Management
            RankToRole();
            GetUsers();
            GetRank();
            GetRegion();
            RankBase();
            RemoveRegion();
            RoleBase();
            RemoveRank();
            RegionBase();
            RemoveRole();
            GetRole();
            //Account Management
            ClaimAccount();
            ClaimAccountBase();
            RemoveAccount();
            GetInfo();
            LeagueInfo();
            AccountToRoles();
            SelfInfo();
            Users();
            //Admin Commands
            SetGame();
            SetStatus();
            ForceClaim();
            BanUser();
            ClearRooms();
            CurrentGame();
            ManualFlair();
            //Team management
            InviteToTeam();
            AcceptInvite();
            CreateTeam();
            //Voice Room
            CreateVoiceroom();
            VoiceRoomCleanup();
            InviteToRoom();
            //Riot API Fuckery
            Status();
            //EventTriggers
            GreetingMessage();
            LeaveLog();
            //Tools
            CounterUsers();
            BotUser.ExecuteAndWait(async () =>
            {
                await BotUser.Connect(APIKeys.discordkey, TokenType.Bot);
            });

        }
        #region logging
        private void Log(string message)
        {
            BotUser.GetChannel(289512073048621056).SendMessage("`"+message+"`");
        }
        private void AdminLog(string message)
        {
            BotUser.GetChannel(ServerIDs.StaffID()).SendMessage(message);
        }
        private void DMBort(string message)
        {
            BotUser.GetServer(ServerIDs.StaffID()).FindUsers("Bort", true).First().SendMessage(message);
        }
        #endregion logging
        private void button1_Click(object sender, EventArgs e)
        {
            
        }
        #region Queue
        private void CheckQueue()
        {
            commands.CreateCommand("CheckQueue")
                .Do(async (e) =>
                {
                    /*Log(e.User + " has asked for the queue in region " + e.GetArg("Region"));
                    await e.Channel.SendMessage(Rito.GetSummonersQueue(SQL.GetQueue(e.GetArg("Region"), SQL.StringToQueueID(e.GetArg("Queue"))),e.GetArg("Region")));*/
                    await e.Channel.SendMessage("This command has been disabled");
                });
        }
        #endregion Queue
        #region Ranks
        private void GetRole()
        {
            commands.CreateCommand("Role")
                .Alias("GetRole")
                .Parameter("Role", ParameterType.Required)
                .Do(async (e) =>
                {
                    string role = e.GetArg("Role").ToLower();
                    if (role == "top" || role == "jungle" || role == "mid" || role == "adc" || role == "support" || role == "fill")
                    {
                        await e.User.AddRoles(e.Server.FindRoles(role, false).First());
                        await e.Channel.SendMessage("Your role has been granted");
                    }
                    else
                    {
                        await e.Channel.SendMessage("Please supply a proper role (Top, Jungle, Mid, ADC, Support or Fill");
                    }
                });
        }

        private void RemoveRole()
        {
            commands.CreateCommand("DeleteRole")
                .Alias("RemoveRole")
                .Parameter("Role", ParameterType.Required)
                .Do(async (e) =>
                {
                    string role = e.GetArg("Role").ToLower();
                    if (role == "top" || role == "jungle" || role == "mid" || role == "adc" || role == "support" || role == "fill")
                    {
                        await e.User.RemoveRoles(e.Server.FindRoles(role, false).First());
                        await e.Channel.SendMessage("Your role has been removed");
                    }
                    else
                    {
                        await e.Channel.SendMessage("Please supply a proper role (Top, Jungle, Mid, ADC, Support or Fill");
                    }
                });
        }
        private void RoleBase()
        {
            commands.CreateCommand("Role")
                .Do(async (e) =>
                {
                    await e.Channel.SendMessage(
                        "Use -Role **<Role>** to get your role assinged.\nValid roles are: Top, Jungle, Mid, ADC, Support, Fill.\nExample: *-Role Support*");
                });
        }

        private void CounterUsers()
        {
            commands.CreateCommand("InRole")
                .Parameter("Role", ParameterType.Required)
                .Do(async (e) =>
                {
                    await e.Channel.SendIsTyping();
                    string returnstring = "Users with the *" + e.GetArg("Role") + "*  Role :\n ";
                    string returnstring2 = "";
                    foreach (User u in e.Server.Users)
                    {
                        if (u.HasRole(e.Server.FindRoles(e.GetArg("Role")).First()) && returnstring.Length <= 1950)
                        {
                            returnstring += u.Name + ", ";
                        }
                        else if (u.HasRole(e.Server.FindRoles(e.GetArg("Role")).First()))
                        {
                            returnstring2 += u.Name + ", ";
                        }   
                    }
                    if (returnstring.Length <= 1960)
                    {
                        await e.Channel.SendMessage(returnstring);
                    }
                    else
                    {
                        await e.Channel.SendMessage(returnstring);
                        await e.Channel.SendMessage(returnstring2);
                    }
                    
                });
        }
        private void Users()
        {
            commands.CreateCommand("Users")
                .Do(async (e) =>
                {
                    await e.Channel.SendMessage("Amount of registered users: " + SQL.CountUsers());
                });
        }
        private void ClaimAccount()
        {
            commands.CreateCommand("ClaimAccount")
                .Parameter("Region", ParameterType.Required)
                .Parameter("SummonerName", ParameterType.Unparsed)

                .Do(async (e) =>
                {
                    
                        User bort = e.Server.Owner;
                        bool verified = false;
                        string returnmessage = "Your account has been claimed!";
                        try
                        {
                            if (Rito.RunePageTest(Rito.NameToID(e.GetArg("SummonerName"), e.GetArg("Region")), e.GetArg("Region")) == true)
                            {
                                verified = true;
                            }
                            else
                            {
                                returnmessage = "Please rename one of your Runepages to " + e.GetArg("SummonerName") + "Atlas";
                            }
                        }
                        catch
                        {
                           returnmessage = 
                                 "Something has failed but your account has been claimed sucessfully.\n" + bort.Mention + " send help pls :c";
                        }
                        if (verified == true)
                        {
                            SQL.ClaimRiotAccount((e.User.Id.ToString()), Rito.NameToID(e.GetArg("SummonerName"), e.GetArg("Region").ToLower()), e.GetArg("Region").ToLower());
                            if (e.User.HasRole(e.Server.GetRole(289510020431740929)))
                            {
                                await e.User.RemoveRoles(e.Server.GetRole(289510020431740929));

                            }
                            if (e.User.HasRole(e.Server.GetRole(289509972927315969)) == false)
                            {
                                await e.User.AddRoles(e.Server.GetRole(289509972927315969));
                            }
                            Log(e.User.ToString() + " has claimed the account " + e.GetArg("SummonerName"));
                            string userid = e.User.Id.ToString();
                            RiotSharp.Region r = Rito.IntToRegion(SQL.GetRegionDiscord(e.User.Id.ToString()));
                            string summonername = Rito.IdToName(SQL.DiscordToRiot(userid), r);
                            Role region = e.Server.FindRoles(Rito.IntToRegion((SQL.GetRegionDiscord(e.User.Id.ToString()))).ToString()).First();
                            try
                            {
                                await e.User.AddRoles(region);
                            }
                            catch { }
                            try
                            {
                                Role rank;
                                if (Rito.GetRank(Rito.IdToName(SQL.DiscordToRiot(userid), r), r.ToString()) != "diamond")
                                {
                                    rank = e.Server.FindRoles(Rito.GetRank(summonername, r.ToString())).First();
                                }
                                else
                                {
                                    rank = e.Server.FindRoles(Rito.GetRankComplicated(summonername, r.ToString())).First();
                                }
                                await e.User.AddRoles(rank);
                            }
                            catch { }
                            try
                            {
                                Role role = e.Server.FindRoles(Rito.GetRole(SQL.DiscordToRiot(userid), r)).First();
                                await e.User.AddRoles(role);
                            }
                            catch { }
                           
                            returnmessage = "Your account has been claimed!";
                            CreateFlair(e.User, e.GetArg("Region"));
                        }
                        if (returnmessage != "Your account has been claimed!")
                        {
                            Log(e.User + " has failed to claim the account  " + e.GetArg("SummonerName") + " in region " + e.GetArg("Region"));
                        }
                        await e.Channel.SendMessage(returnmessage);
                    
                }
                );
        }

        private void ClaimAccountBase()
        {
            commands.CreateCommand("ClaimAccount")
                .Do(async (e) =>
                {
                    await e.Channel.SendMessage(
                        "Use -ClaimAccount **<Region> <Username>** to claim your account in our system and get your roles assigned.\nExample: *-ClaimAccount EUW BortTheBeaver*");
                });
        }
        private void ForceClaim()
        {
            commands.CreateCommand("ForceAccount")
                .Parameter("Region", ParameterType.Required)
                .Parameter("SummonerName", ParameterType.Required)
                .Parameter("User", ParameterType.Required)
                .Do(async (e) =>
                {
                    User bort = e.Server.Owner;
                    if(SQL.AdminCheck(e.User.Id.ToString())== true){
                        SQL.ClaimRiotAccount((e.Message.MentionedUsers.First().Id.ToString()), Rito.NameToID(e.GetArg("SummonerName"), e.GetArg("Region")), e.GetArg("Region"));
                        try
                        {

                            string userid = e.Message.MentionedUsers.First().Id.ToString();
                            RiotSharp.Region r = Rito.IntToRegion(SQL.GetRegionDiscord(e.User.Id.ToString()));
                            string summonername = Rito.IdToName(SQL.DiscordToRiot(userid), r);
                            Role region =
                                e.Server.FindRoles(
                                    Rito.IntToRegion((SQL.GetRegionDiscord(e.User.Id.ToString()))).ToString()).First();
                            Role rank;
                            if (Rito.GetRank(Rito.IdToName(SQL.DiscordToRiot(userid), r), r.ToString()) != "diamond")
                            {
                                rank = e.Server.FindRoles(Rito.GetRank(summonername, r.ToString())).First();
                            }
                            else
                            {
                                rank = e.Server.FindRoles(Rito.GetRankComplicated(summonername, r.ToString())).First();
                            }
                            Role role = e.Server.FindRoles(Rito.GetRole(SQL.DiscordToRiot(userid), r)).First();
                            await e.Message.MentionedUsers.First().AddRoles(region, rank, role);
                            Log(e.User.ToString() + " has claimed the account " + e.GetArg("SummonerName") + " for " +
                                e.Message.MentionedUsers.First());
                            CreateFlair(e.Message.MentionedUsers.First(), e.GetArg("Region"));
                        }
                        catch
                        {
                            await e.Channel.SendMessage(
                                "Something has failed but your account has been claimed sucessfully.\n" + bort.Mention + " send help pls :c");
                        }
                    }
                });
        }
        private void RemoveAccount()
        {
            string returnmessage = "your account has been removed!";
            commands.CreateCommand("RemoveAccount")
            .Do(async (e) =>
             {
                
                     try
                     {
                         SQL.RemoveRiotAccount(e.User.Id.ToString());
                     }
                     catch
                     {
                         returnmessage = "An error has occured";
                     }
                     Log(e.User + " has removed his account");
                     await e.Channel.SendMessage(returnmessage);
                 
             }
             );
        }
        private void RankToRole()
        {
            commands.CreateCommand("GetRoles")
                .Parameter("Region", ParameterType.Required)
                .Parameter("SummonerName", ParameterType.Unparsed)
                .Do(async (e) =>
                {
                        try
                        {
                            RiotSharp.Region r = Rito.StringToRegion(e.GetArg("Region").ToLower());
                            string summonername = e.GetArg("SummonerName");
                            Role region = e.Server.FindRoles(r.ToString().ToLower()).First();
                            await e.User.AddRoles(region);
                            Role rank;
                            if (Rito.GetRank(summonername, r.ToString()).ToString() != "diamond")
                            {
                                rank = e.Server.FindRoles(Rito.GetRank(summonername, r.ToString())).First();
                            }
                            else
                            {
                                rank = e.Server.FindRoles(Rito.GetRankComplicated(summonername, r.ToString())).First();
                            }
                            await e.User.AddRoles(rank);
                            Role role = e.Server.FindRoles(Rito.GetRole(Rito.NameToID(summonername, r.ToString()), r)).First();
                            await e.User.AddRoles(role);
                            await e.Channel.SendMessage("Your role has been added!");
                        }
                        catch { }
                }
                );
        }

        private void RankBase()
        {
            commands.CreateCommand("Rank")
                .Do(async (e) =>
                {
                    await e.Channel.SendMessage(
                        "Use -Rank **<Rank>** to get your rank assinged.\nValid ranks include Bronze, Silver, Gold, Platinum, Diamond X, Master\nExample: *-Rank Gold*");
                });
        }
        private void AccountToRoles()
        {
            commands.CreateCommand("GetRoles")
                .Do(async (e) =>
                {
                    try
                    {
                        string userid = e.User.Id.ToString();
                        RiotSharp.Region r = Rito.IntToRegion(SQL.GetRegionDiscord(e.User.Id.ToString()));
                        string summonername = Rito.IdToName(SQL.DiscordToRiot(userid), r);
                        Role region = e.Server.FindRoles(Rito.IntToRegion((SQL.GetRegionDiscord(e.User.Id.ToString()))).ToString()).First();
                        Role rank;
                        if (Rito.GetRank(Rito.IdToName(SQL.DiscordToRiot(userid), r), r.ToString()) != "diamond")
                        {
                            rank = e.Server.FindRoles(Rito.GetRank(summonername, r.ToString())).First();
                        }
                        else
                        {
                            rank = e.Server.FindRoles(Rito.GetRankComplicated(summonername, r.ToString())).First();
                        }
                        Role role = e.Server.FindRoles(Rito.GetRole(SQL.DiscordToRiot(userid), r)).First();
                        await e.User.AddRoles(region, rank, role);

                    }
                    catch { }
                    
                    await e.Channel.SendMessage("Your roles have been added");
                });
        }
        private void GetRank()
        {
            commands.CreateCommand("Rank")
                .Parameter("Rank", ParameterType.Required)
                .Parameter("Division", ParameterType.Optional)
                .Do(async (e) =>
                {
                   


                        string rank = e.GetArg("Rank");
                        if (rank.ToLower().Contains("diamond") == true)
                        {
                            try
                            {
                                string division = e.GetArg("Division");
                                if (e.GetArg("Division") == "5" || e.GetArg("Division") == "4" || e.GetArg("Division") == "3" || e.GetArg("Division") == "2" || e.GetArg("Division") == "1")
                                {
                                    division = NumberToRoman(Convert.ToInt32(division));
                                }
                                await e.User.AddRoles(e.Server.FindRoles(rank + " " + division).First());
                                await e.Channel.SendMessage("Role " + rank.ToString() + " has been succesfully added");
                                Log(e.User.ToString() + " has claimed the role " + rank.ToString());
                            }
                            catch
                            {
                                await e.Channel.SendMessage("Please write your rank as *Diamond V*");
                            }
                        }
                        else
                        {
                            foreach (Atlas.Rank r in Enum.GetValues(typeof(Atlas.Rank)).Cast<Atlas.Rank>())
                            {
                                try
                                {
                                    if (e.User.HasRole(e.Server.FindRoles(r.ToString(), false).First()) == true)
                                    {
                                        await e.User.RemoveRoles(e.Server.FindRoles(r.ToString(), false).First());
                                    }
                                    if (rank.ToLower() == r.ToString().ToLower())
                                    {
                                        await e.User.AddRoles(e.Server.FindRoles(r.ToString()).First());
                                        await e.Channel.SendMessage("Role " + r.ToString() + " has been succesfully added");
                                        Log(e.User.ToString() + " has claimed the role: " + r.ToString());
                                    }

                                }
                                catch
                                {

                                }
                            }
                        
                        if (e.User.HasRole(e.Server.GetRole(289509972927315969)) == false && e.User.HasRole(e.Server.GetRole((289510020431740929))) == false)
                        {
                            await e.User.AddRoles(e.Server.GetRole(289510020431740929));
                        }
                    }
                });
        }

        private void RemoveRank()
        {
            commands.CreateCommand("RemoveRank")
                .Do(async (e) =>
                {

                    foreach (Atlas.Rank r in Enum.GetValues(typeof(Atlas.Rank)).Cast<Atlas.Rank>())
                    {
                        try
                        {
                            if (e.User.HasRole(e.Server.FindRoles(r.ToString(), false).First()) == true)
                            {
                                await e.User.RemoveRoles(e.Server.FindRoles(r.ToString(), false).First());
                                await e.Channel.SendMessage("Removed the " + r.ToString() + "role for you.");
                            }

                        }
                        catch { }
                    }
                    foreach (Role r in e.User.Roles)
                    {
                        if (r.ToString().ToLower().Contains("diamond"))
                        {
                            await e.User.RemoveRoles(r);
                        }
                    }
                });
        }
        private void GetRegion()
        {
            commands.CreateCommand("Region")
                .Parameter("Region", ParameterType.Required)
                .Do(async (e) =>
                {
                    string returnstring = "Please give a valid LoL Region";
                    string region = e.GetArg("Region");
                    foreach (RiotSharp.Region r in Enum.GetValues(typeof(RiotSharp.Region)).Cast<RiotSharp.Region>())
                    {
                        try
                        {
                            if (e.User.HasRole(e.Server.FindRoles(r.ToString(), false).First()) == true)
                            {
                                await e.User.RemoveRoles(e.Server.FindRoles(r.ToString(), false).First());
                            }
                            if (region.ToLower() == r.ToString())
                            {
                                await e.User.AddRoles(e.Server.FindRoles(r.ToString()).First());
                                returnstring = "Added the " + r.ToString().ToLower() + " role to you.";
                                
                                Log(e.User.ToString() + " has claimed the role: " + r.ToString().ToLower());
                            }
                        }
                        catch
                        {
                            
                        }

                    }
                    if (e.User.HasRole(e.Server.GetRole(289509972927315969)) == false && e.User.HasRole(e.Server.GetRole((289510020431740929))) == false)
                    {
                        await e.User.AddRoles(e.Server.FindRoles("unverified" ,false).First());
                    }
                    await e.Channel.SendMessage(returnstring);
                }
                );
        }
        private void RegionBase()
        {
            commands.CreateCommand("Region")
                .Do(async (e) =>
                {
                    await e.Channel.SendMessage(
                        "Use -Region **<Region>** to get your region assinged. \nValid regions are: EUW, NA, LAS, LAN, BR, OCE, EUNE.\nExample: *-Region EUW*");
                });
        }
        private void RemoveRegion()
        {
            commands.CreateCommand("ClearRegion")
                .Parameter("Region", ParameterType.Required)
                .Alias("RemoveRegions")
                .Alias("RemoveRegion")
                .Do(async (e) =>
                {
                    foreach (RiotSharp.Region r in Enum.GetValues(typeof(RiotSharp.Region)).Cast<RiotSharp.Region>())
                    {
                        if (e.User.HasRole(e.Server.FindRoles(r.ToString(), false).First()) == true)
                        {
                            await e.Channel.SendMessage("Remove the " + r.ToString() + " role from you.");
                            await e.User.RemoveRoles(e.Server.FindRoles(r.ToString(),false).First());
                        }
                    }
                });
        }
        #endregion Ranks
        private void GetInfo()
        {
            commands.CreateCommand("GetInformation")
                .Alias("GetInfo")
                .Alias("Info")
                .Parameter("User", ParameterType.Required)
          .Do(async (e) =>
          {
              Log(e.User + " has asked for the information of " + e.Message.MentionedUsers.First().Name);
              await e.Channel.SendIsTyping(); 
              string userid = e.Message.MentionedUsers.First().Id.ToString();
              await e.Channel.SendMessage(Rito.GetGame(SQL.DiscordToRiot(userid),Rito.IntToRegion(SQL.GetRegionDiscord(userid)),SQL.GetRegionDiscord(userid)));
          }
          );
        }

        private void CurrentGame()
        {
            commands.CreateCommand("Game")
                .Parameter("Region", ParameterType.Required)
                .Parameter("SummonerName", ParameterType.Unparsed)
                .Do(async (e) =>
                {
                    await e.Channel.SendMessage(Rito.GetCurrentGame(Rito.StringToRegion(e.GetArg("Region")),
                        e.GetArg("SummonerName")));
                });
        }
        private void LeagueInfo()
        {
            commands.CreateCommand("Info")
                .Parameter("Region", ParameterType.Required)
                .Parameter("SummonerName", ParameterType.Unparsed)
                .Do(async (e) =>
                {
                    await e.Channel.SendIsTyping();
                    await e.Channel.SendMessage(
                        Rito.GetGame(Rito.NameToID(e.GetArg("SummonerName"), e.GetArg("Region")), Rito.StringToRegion(e.GetArg("Region")),
                            Rito.StringToRegionInt(e.GetArg("Region"))));
                });
        }
        private void SelfInfo()
        {
            commands.CreateCommand("Info")
                .Do(async (e) =>
                {
                    await e.Channel.SendIsTyping();
                    await e.Channel.SendMessage(Rito.GetGame(SQL.DiscordToRiot(e.User.Id.ToString()), Rito.IntToRegion(SQL.GetRegionDiscord(e.User.Id.ToString())), SQL.GetRegionDiscord(e.User.Id.ToString())));
                });
        }
        #region Teams
        private void CreateTeam()
        {
            commands.CreateCommand("CreateTeam")
                .Parameter("Name", ParameterType.Required)
                .Do(async (e) =>
                {
                    string returnstring = "Your team has been created";
                    try
                    {
                        SQL.CreateTeam(e.GetArg("Name"), e.Message.User.Id.ToString());
                    }
                    catch
                    {
                        returnstring = "An error has occured";
                    }
                    if (returnstring == "Your team has been created")
                    {
                        Log(e.User + " has created a team called " + e.GetArg("Name"));
                    }
                    else
                    {
                        Log(e.User + " has failed to create a team called " + e.GetArg("Name"));
                    }
                    await e.Channel.SendMessage(returnstring);
                }
                );
        }
        private void InviteToTeam()
        {
            commands.CreateCommand("Invite")
                .Parameter("Name", ParameterType.Required)
                .Do(async (e) =>
                {
                if (SQL.InviteToTeam(e.User.Id.ToString(), SQL.DiscordIDtoUserID(e.Message.MentionedUsers.First().Id.ToString())) == true)
                    {
                        await e.Channel.SendMessage("User has been invited to your team!");
                        await e.Message.MentionedUsers.First().SendMessage(e.User.Name + " has invited you to their team " + SQL.IDToTeamName(SQL.OwnerToTeamID(e.User.Id.ToString())) + " please do command !AcceptInvite " + SQL.GetIDInvite(e.Message.MentionedUsers.First().Id.ToString(), SQL.IDToTeamName(SQL.OwnerToTeamID(e.User.Id.ToString()))) + " to accept the invite");
                    }
                    else
                    {
                        await e.Channel.SendMessage("There was an error, user has not been invited");
                    }
                }
                );
        }
        private void AcceptInvite()
        {
            commands.CreateCommand("AcceptInvite")
                .Parameter("ID", ParameterType.Required)
                .Do(async (e) =>
                {
                    SQL.AcceptInvite(e.User.Id.ToString(), Convert.ToInt32(e.GetArg("ID")));
                    await e.Channel.SendMessage("You have joined the team successfully");
                    Log(e.User + " has joined the team with the ID " + e.GetArg("ID"));
                }
                );
        }
        #endregion Teams

        private void Help()
        {
            commands.CreateCommand("Help")
                .Do(async (e)=>
                {
                    await e.User.SendMessage(
                        "**Account Claiming:**\n-ClaimAccount <Region> <Summonername>: Claims your account within our system and gives you the right roles" +
                        "\n-RemoveAccount: Removes your account out of our database" +
                        "\n-Info: Gives you information about your account. This account can also be used when tagging other people like -Info @Bort." +
                        "\nUsing -Info <Region><Summonername> will give you the information about that account as well." +
                        "\n\n**Role Commands:**\n-GetRoles <Region> <Summonername>: Gets the roles that belong to your summoner without claiming it" +
                        "\n-Role <Role> Gives you the role that you have given" +
                        "\n-Region <Region> Assigns the role that comes with your region" +
                        "\n-Rank <Rank> Assigns the role that comes with your rank\n\n");
                    await e.User.SendMessage("**Voice Commands:**" +
                                             "\n-CreateVoice <@someone>: Makes a private Voice chat room for you and your friend that will be removed once its empty." +
                                             "\n-InviteVoice <@someone>: Allows the tagged person to enter your personal voice room");
                    await e.Channel.SendMessage("DMed the command list to you!");

                });
        }
        private void Test()
        {
            commands.CreateCommand("Test")
         .Do(async (e) =>
                    {
                        await e.Channel.SendIsTyping();
             await e.Channel.SendMessage(Rito.GetGame(SQL.DiscordToRiot(e.User.Id.ToString()), Rito.IntToRegion((SQL.GetRegionDiscord(e.User.Id.ToString()))), SQL.GetRegionDiscord(e.User.Id.ToString())));
         }
         );
        }
        private void ManualFlair()
        {
            commands.CreateCommand("CreateFlair")
                .Parameter("Name", ParameterType.Required)
                .Parameter("Region", ParameterType.Required)
                .Do(async (e) =>
                {
                    if (SQL.AdminCheck(e.Message.User.Id.ToString()))
                    {
                        CreateFlair(e.Message.MentionedUsers.First(), e.GetArg("Region"));
                        await e.Channel.SendMessage("Flair should be created :thinking:");
                    }
                }
                );
        }
        private void MessagesUser(ulong id, string message)
        {
            User u = BotUser.GetServer(ServerIDs.AtlasID()).FindUsers("Bort", true).First();
            foreach (Server s in BotUser.Servers)
            {
                try
                {
                    u = s.GetUser(id);
                }
                catch { }
            }
            u.SendMessage(message);
        }
        /*
        private void DuoFinder()
        {
            try
            {
                for (int i = 0; i < duos.Count; i++)
                {
                    for (int x = duos.Count; x >= 0; x--)
                    {
                        if (duos[i].CheckCompatability(duos[x].GetRank, duos[x].GetRegion, duos[x].DiscordID) == true && duos[i].DiscordID != duos[x].DiscordID && duos[i].Invite == false && duos[x].Invite == false)
                        {
                            User Invited = IDToUser(duos[x].DiscordID);
                            MessagesUser(duos[i].DiscordID, "A User has been found for you : \n" + Rito.GetInformation(SQL.DiscordToRiot(duos[x].DiscordID.ToString()), SQL.GetRegionDiscord(duos[x].DiscordID.ToString())));
                            duos[i].Invite = true;
                            duos[x].Invite = true;
                            duos[i].InvitedID = duos[x].DiscordID;
                            duos[x].InvitedID = duos[i].DiscordID;
                        }
                    }
                }
            }
            catch { }
            
        }

        private void AcceptDuoInvite()
        {
            string returnstring = "You don't have any open invites";
            commands.CreateCommand("AcceptInvite")
                .Alias("Accept")
                .Do(async (e) =>
                {
                    foreach(Duo d in duos)
                    {
                    if (d.DiscordID == e.User.Id && d.Invite == true && d.Accepted == false && d.InviteAccepted == false) 
                        {
                            d.Accepted = true;
                            returnstring = "You have accepted your part of the invite";
                        }
                        else if (d.DiscordID == e.User.Id && d.Invite == true && d.Accepted == false && d.InviteAccepted == true)
                        {
                            d.Accepted = true;
                            returnstring = "You have both accept your invite, good luck on the rift!";
                            duos.Remove(d);
                        }
                        else if (d.DiscordID == e.User.Id && d.Invite == true && d.Accepted == true)
                        {
                            returnstring = "You have already accepted the invite, please be patient";
                        }
                        if (d.InvitedID == e.User.Id && d.InviteAccepted == false && d.Invite == true)
                        {
                            d.InviteAccepted = true;
                        }
                    }
                   
                    await e.Channel.SendMessage(returnstring);
                });
        }
        private void RejectDuoInvite()
        {
            string returnstring = "you don't have any open invites";
            commands.CreateCommand("RejectInvite")
                .Alias("Decline", "Reject")
                .Do(async (e) =>
                {
                    try
                    {
                        foreach (Duo d in duos)
                        {
                            if (d.DiscordID == e.User.Id && d.Invite == true)
                            {
                                d.Invite = false;
                                d.InviteAccepted = false;
                                d.Accepted = false;
                                d.RejectUser(d.InvitedID);
                                returnstring = "Your invite has been declined successfully";
                            }
                            if (d.InvitedID == e.User.Id && d.Invite == true)
                            {
                                d.Invite = false;
                                d.InviteAccepted = false;
                                d.Accepted = false;
                                d.RejectUser(d.InvitedID);
                            }
                        }
                    }
                    catch { }
                    await e.Channel.SendMessage(returnstring);
                });
        }
        */
        private User IDToUser(ulong id)
        {
            User u = BotUser.GetServer(ServerIDs.TestID()).FindUsers("Bort", false).First();
            foreach (Server s in BotUser.Servers)
            {
                try
                {
                    u = s.GetUser(id);
                }
                catch { }
            }
            return u;
        }
        
        #region EventTriggers
        private void GreetingMessage()
        {
            BotUser.UserJoined += async (s, u) =>
            {
                if (u.Server.Id == ServerIDs.AtlasID())
                {
                    Log(u.User.ToString() + " has joined Atlas");
                    await u.User.SendMessage("Welcome to the Atlas Matchmaking server, we recommend you to read #Rules-and-Info before you go any further. \n \n You can use my commands get the roles you need to get started. Claim your account or set your Region to begin the matchmaking experience, look in the prementioned channel on how to do this. \n I will receive multiple updates so look for the announcements to chech them out! \n \n Goodluck on the Rift!");
                }
            };
        }
        private void LeaveLog()
        {
            BotUser.UserLeft += async (s, u) =>
            {
                if (u.Server.Id == ServerIDs.AtlasID())
                {
                    Log(u.User.ToString() + " has left Atlas");
                    await u.User.SendMessage("You left our server, please concider giving feedback why.");
                }
            };
        }
    
        private void CreateFlair(User u, string region)
        {
            Server s = BotUser.GetServer(ServerIDs.AtlasID());
            Channel c = s.FindChannels(region + "-flairs", ChannelType.Text, false).First();
            bool edited = false;
            foreach (Discord.Message m in c.Messages)
            {
                if (m.Text.Contains(u.Name.ToString()) == true)
                {
                    edited = true;
                    m.Edit(u.Mention + " " + Rito.GetGame(SQL.DiscordToRiot(u.Id.ToString()),Rito.IntToRegion(SQL.GetRegionDiscord(u.Id.ToString())) ,SQL.GetRegionDiscord(u.Id.ToString())));
                }
            }
            if (edited == false)
            {
                c.SendMessage(u.Mention + " " + Rito.GetGame(SQL.DiscordToRiot(u.Id.ToString()),Rito.IntToRegion(SQL.GetRegionDiscord(u.Id.ToString())) ,SQL.GetRegionDiscord(u.Id.ToString())));
            }
        }
        private void VoiceRoomCleanup()
        {
            BotUser.UserUpdated += async (s, u) =>
            {
                try
                {
                    if (u.Before.VoiceChannel.Name.Contains("Pers channel") == true)
                    {
                        foreach (var vc in u.Server.VoiceChannels)
                        {
                            if (vc.Users.Count() == 0 && vc.Name == u.Before.VoiceChannel.Name)
                            {
                                await vc.Delete();
                                Log("The server has deleted the voice channel " + vc.Name);
                            }
                        }
                    }
                }
                catch { }
            };
        }
        #endregion EventTriggers
        private void Status()
        {
            commands.CreateCommand("Status")
                .Alias("GetStatus")
                .Parameter("Region", ParameterType.Required)
                .Description("Check a servers status")
                .Do(async (e) =>
                {
                    Log(e.User.ToString() + " has checked for the status of the region: " + e.GetArg("Region"));
                    await e.Channel.SendMessage(Rito.ServerStatus(Rito.StringToRegion(e.GetArg("Region"))));
                }
                );
        }
        #region VoiceRooms
        private void CreateVoiceroom()
        {
            commands.CreateCommand("CreateRoom")
                .Alias("VCRoom", "CreateVoice")
                .Parameter("User", ParameterType.Multiple)
                .Do(async (e) =>
                {
                    bool createchannel = true;
                    foreach (var vc in e.Server.VoiceChannels)
                    {
                        if (vc.Name == "Pers channel " + e.User.Name)
                        {
                            createchannel = false;
                        }
                    }
                    if (createchannel == true)
                    {
                        ulong id = e.Server.CreateChannel("Pers channel " + e.User.Name, ChannelType.Voice).Result.Id;
                        Channel channel = e.Server.GetChannel(id);
                        ChannelPermissions test = ChannelPermissions.VoiceOnly;
                        ChannelPermissions test2 = ChannelPermissions.None;
                        ChannelPermissions test3 = new ChannelPermissions(ChannelPermissions.VoiceOnly, false, false, null, null, null, null, null, null, null, null, true, true, false, false, false, true, false);
                        await channel.AddPermissionsRule(e.Server.FindRoles("verified").First(), test2, test);
                        await channel.AddPermissionsRule(e.User, test3, test2);
                        await e.Channel.SendMessage("channel has been created");
                        Log(e.User.ToString() + " has created a voice room");
                        foreach (User u in e.Message.MentionedUsers)
                        {
                            await channel.AddPermissionsRule(u, test3, test2);
                        } 
                    }
                    else
                    {
                        await e.Channel.SendMessage("A channel with your name already excists, please join that one");
                    }
                }
                );
        }
        private void InviteToRoom()
        {
            commands.CreateCommand("InviteVoice")
                .Alias("VCIvite", "VoiceInvite")
                .Parameter("User", ParameterType.Multiple)
                .Do(async (e) =>
                {
                    Channel c = e.User.VoiceChannel;
                    if (c.Name.Contains("Pers channel " + e.User.Name.First()) == true)
                    {
                        ChannelPermissions test2 = ChannelPermissions.None;
                        ChannelPermissions test3 = new ChannelPermissions(ChannelPermissions.VoiceOnly, false, false, null, null, null, null, null, null, null, null, true, true, false, false, false, true, false);
                        foreach (User u in e.Message.MentionedUsers)
                        {
                            await c.AddPermissionsRule(u, test3, test2);
                        }
                        await e.Channel.SendMessage("User(s) have been invited");
                    }
                   
                }
                );
        }
        #endregion VoiceRooms
        #region AdminCommands
        private void SetGame()
        {
            commands.CreateCommand("SetGame")
                .Parameter("Game", ParameterType.Required)
                .Do(async (e) => 
                {
                    if (SQL.AdminCheck(e.Message.User.Id.ToString()))
                    {
                        BotUser.SetGame(e.GetArg("Game"));
                        Log(e.Message + " set the game to " + e.GetArg("Game"));
                        await e.User.SendMessage("Game has been set!");
                    }
                    else
                    {
                        Log(e.Message + " failed to set the game to " + e.GetArg("Game"));
                        await e.Channel.SendMessage(e.User.Mention + ", you do not have permission to change the game");
                    }
                }
                );
        }
        private void SetStatus()
        {
            commands.CreateCommand("SetStatus")
                .Parameter("Status", ParameterType.Required)
                .Do(async (e) =>
                {
                    if (SQL.AdminCheck(e.Message.User.Id.ToString()))
                    {
                        if (e.GetArg("Status").ToLower() == "online")
                        {
                            BotUser.SetStatus(UserStatus.Online);
                        }
                        else if (e.GetArg("Status").ToLower() == "do not disturb")
                        {
                            BotUser.SetStatus(UserStatus.DoNotDisturb);
                        }
                        else if (e.GetArg("Status").ToLower() == "idle" || e.GetArg("Status").ToLower() == "away")
                        {
                            BotUser.SetStatus(UserStatus.Idle);
                        }
                        else if (e.GetArg("Status").ToLower() == "ofline" || e.GetArg("Status").ToLower() == "invisible")
                        {
                            BotUser.SetStatus(UserStatus.Invisible);
                        }
                        await e.User.SendMessage("Status has been set");
                        Log(e.User + " has changed the status to " + e.GetArg("Status"));
                    }
                    else
                    {
                        Log(e.User + " failed to set the status to " + e.GetArg("Status"));
                        await e.Channel.SendMessage(e.User.Mention + ", you do not have have permission to change the status");
                    }
                }
                );
        }
        private void ClearRooms()
        {
            commands.CreateCommand("ClearPersChannels")
         .Do(async (e) =>
         {
             if (SQL.AdminCheck(e.Message.User.Id.ToString()))
             {
                 Server s = BotUser.GetServer(285460149769076737);
                 foreach (var vc in s.VoiceChannels)
                 {
                     if (vc.Name.Contains("Pers channel") && vc.Users.Count() == 0)
                     {
                         await vc.Delete();
                         Log(e.User + " has deleted the voice channel " + vc.Name);

                     }
                 }
             }
             else
             {
                 Log(e.User + " tried to remove the voicechannels");
                 await e.User.SendMessage("You do not have permissions to remove the voice channels, message an admin if you should have these permissions");
             }

         }
         );
        }
        private void BanUser()
        {
            commands.CreateCommand("BanUser")
                .Parameter("User", ParameterType.Required)
                .Parameter("Reason", ParameterType.Required)
                .Do(async (e) =>
                {
                    if (SQL.AdminCheck(e.User.Id.ToString()) == true)
                    {
                        Server s = e.Server;
                        await s.Ban(e.Message.MentionedUsers.First(), 0);
                        Log(e.User + " has banned " + e.Message.MentionedUsers.First().ToString());
                        await e.Channel.SendMessage("User has been banned");
                        SQL.BanUser(e.Message.MentionedUsers.First().Id.ToString(), e.Message.MentionedUsers.First().Name, e.GetArg("Reason"), e.User.Name);
                        await e.Message.MentionedUsers.First().SendMessage("You have been banned for " + e.GetArg("Reason") + " by " + e.User.Name + ". if you want to repeal your ban please message Bort#9703.");
                    }
                });
        }
        private void GetUsers()
        {
            commands.CreateCommand("GetBans")
                .Do(async (e) =>
                {
                    if (SQL.AdminCheck(e.User.Id.ToString()) == true)
                    {
                        await e.Channel.SendMessage(SQL.BannedUsers());
                    }
                });
        }
        #endregion AdminCommands
        #region AdminTools
        #endregion
        private string NumberToRoman(int number)
        {
            if (number == 1)
            {
                return "I";
            }
            else if (number == 2)
            {
                return "II";
            }
            else if (number == 3)
            {
                return "III";
            }
            else if (number == 4)
            {
                return "IV";
            }
            else if (number == 5)
            {
                return "V";
            }
            return "error";
        }
    }
}
