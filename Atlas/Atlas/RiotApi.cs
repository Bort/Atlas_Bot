﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using RiotSharp;
using RiotSharp.CurrentGameEndpoint.Enums;
using RiotSharp.MatchEndpoint.Enums;

namespace Atlas
{
    class RiotApiHandler
    {
        private DatabaseHandler SQL = new DatabaseHandler();
        public string GetRank(string SummonerName, string region)
        {
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(StringToRegion(region), SummonerName);
            try
            {
                var stats = summoner.GetLeagues();
                foreach (var item in stats)
                {
                    if (item.Queue == Queue.RankedSolo5x5)
                    {
                        return item.Tier.ToString();
                    }
                }
            }
            catch (RiotSharpException)
            {
                return "error";
            }
            return "error";
        }
        public string GetRankComplicated(string summonername, string region)
        {
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(StringToRegion(region), summonername);
            try
            {
                var stats = summoner.GetLeagues();
                foreach (var item in stats)
                {
                    if (item.Queue == Queue.RankedSolo5x5)
                    {
                        return item.Tier.ToString() + " " + item.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single();
                    }
                }
            }
            catch (RiotSharpException)
            {
                return "error";
            }
            return "error";
        }
        public int NameToID(string SummonerName, string region)
        {
            int id;
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(StringToRegion(region), SummonerName);
            try
            {
                id = Convert.ToInt32(summoner.Id.ToString());
            }
            catch
            {
                id = 0;
            }
            return id;
        }
        public string IdToName(int id, Region r)
        {
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(r, Convert.ToInt64(id));
            return summoner.Name;
        }
        public bool RunePageTest(int RiotID, string region)
        {
            bool login = false;
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(StringToRegion(region), RiotID);
            foreach (var Runepage in summoner.GetRunePages())
            {
                if (Runepage.Name == summoner.Name + "Atlas")
                {
                    login = true;
                }
            } 
            return login;
        }
        public Rank RiotToRank(int id, Region r)
        {
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(r, id);
            foreach(var stat in summoner.GetLeagues())
            {
                if (stat.Queue == Queue.RankedSolo5x5)
                {
                    if (stat.Tier == RiotSharp.LeagueEndpoint.Enums.Tier.Bronze)
                    {
                        return Rank.bronze;
                    }
                    else if (stat.Tier == RiotSharp.LeagueEndpoint.Enums.Tier.Silver)
                    {
                        return Rank.silver;
                    }
                    else if (stat.Tier == RiotSharp.LeagueEndpoint.Enums.Tier.Platinum)
                    {
                        if (stat.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single().ToString() == "I")
                        {
                            return Rank.platniumI;
                        }
                        else if (stat.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single().ToString() == "II")
                        {
                            return Rank.platinumII;
                        }
                        else if (stat.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single().ToString() == "III")
                        {
                            return Rank.platinumIII;
                        }
                        else
                        {
                            return Rank.platinum;
                        }
                    }
                    else if (stat.Tier == RiotSharp.LeagueEndpoint.Enums.Tier.Diamond)
                    {
                        if (stat.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single().ToString() == "V")
                        {
                            return Rank.diamondV;
                        }
                        else if (stat.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single().ToString() == "IV")
                        {
                            return Rank.diamondIV;
                        }
                        else if (stat.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single().ToString() == "III")
                        {
                            return Rank.diamondIII;
                        }
                        else if (stat.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single().ToString() == "II")
                        {
                            return Rank.diamondII;
                        }
                        else if (stat.Entries.Where(x => x.PlayerOrTeamId == summoner.Id.ToString()).Select(x => x.Division).Single().ToString() == "I")
                        {
                            return Rank.diamondI;
                        }
                    }
                    else if (stat.Tier == RiotSharp.LeagueEndpoint.Enums.Tier.Master)
                    {
                        return Rank.master;
                    }
                    else if (stat.Tier == RiotSharp.LeagueEndpoint.Enums.Tier.Challenger)
                    {
                        return Rank.challenger;
                    }
                    else if (stat.Tier == RiotSharp.LeagueEndpoint.Enums.Tier.Unranked)
                    {
                        return Rank.unranked;
                    }
                }
            }
            return Rank.unranked;
        }
        public string GetGame(int RiotID, Region r, int region)
        {
            string returnstring = "";
            var staticApi = StaticRiotApi.GetInstance(APIKeys.riotkey);
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(r, RiotID);
            returnstring += "**" +summoner.Name + ": **";
            if (summoner.Level == 30)
            {
                try
                {
                    string rank;
                    string division;
                    var stats = summoner.GetLeagues();
                    returnstring += "\n**Rankings: **";
                    foreach (var item in stats)
                    {
                        if (item.Queue == Queue.RankedSolo5x5)
                        {
                            rank = item.Tier.ToString();
                            division = item.Entries.Where(y => y.PlayerOrTeamId == summoner.Id.ToString()).Select(y => y.Division).Single();
                            string lp =
                                item.Entries.Where(y => y.PlayerOrTeamId == summoner.Id.ToString())
                                    .Select(y => y.LeaguePoints)
                                    .Single()
                                    .ToString();
                            returnstring += "\nSolo/Duo Queue: " + rank + " " + division + ": " + lp + " LP";

                        }
                        else if (item.Queue == Queue.RankedFlexSR)
                        {
                            rank = item.Tier.ToString();
                            division = item.Entries.Where(y => y.PlayerOrTeamId == summoner.Id.ToString()).Select(y => y.Division).Single();
                            string lp =
                                item.Entries.Where(y => y.PlayerOrTeamId == summoner.Id.ToString())
                                    .Select(y => y.LeaguePoints)
                                    .Single()
                                    .ToString();

                            returnstring += "\n5v5 Flex Queue: " + rank + " " + division + ": " + lp + " LP";
                        }
                        else if (item.Queue == Queue.RankedFlexTT)
                        {
                            rank = item.Tier.ToString();
                            division = item.Entries.Where(y => y.PlayerOrTeamId == summoner.Id.ToString()).Select(y => y.Division).Single();
                            string lp =
                                item.Entries.Where(y => y.PlayerOrTeamId == summoner.Id.ToString())
                                    .Select(y => y.LeaguePoints)
                                    .Single()
                                    .ToString();
                            returnstring += "\n3v3 Flex Queue: " + rank + " " + division + ": " + lp + " LP";
                        }
                    }
                }
                catch
                {

                }
            }
            DateTime starts7 = new DateTime(2016,12,7);
            try
            {
                var matches = api.GetMatchList(r, RiotID, null, null, null, starts7).Matches;
                List<Champ> champs = new List<Champ>();
                int[] roles = new int[5] { 0, 0, 0, 0, 0 };
                foreach (var match in matches)
                {
                    try
                    {
                        if (match.Queue == Queue.RankedSolo5x5 || match.Season.ToString().Contains("Pre"))
                        {
                            if (match.Lane == Lane.Top)
                            {
                                roles[0] += 1;
                            }
                            else if (match.Lane == Lane.Jungle)
                            {
                                roles[1] += 1;
                            }
                            else if (match.Lane == Lane.Mid || match.Lane == Lane.Middle)
                            {
                                roles[2] += 1;
                            }
                            else if (match.Lane == Lane.Bot || match.Lane == Lane.Bottom && match.Role == Role.DuoCarry)
                            {
                                roles[3] += 1;
                            }
                            else if (match.Lane == Lane.Bot || match.Lane == Lane.Bottom && match.Role == Role.DuoSupport)
                            {
                                roles[4] += 1;
                            }
                            bool found = false;
                            if (champs.Count != 0)
                            {
                                foreach (Champ c in champs)
                                {
                                    if (c.Id == match.ChampionID)
                                    {
                                        c.IncreaseAmount();
                                        found = true;
                                    }
                                }
                            }
                            if (found == false)
                            {
                                champs.Add(new Champ(staticApi.GetChampion(r, Convert.ToInt32(match.ChampionID)).Name, Convert.ToInt32(match.ChampionID)));
                            }

                        }
                    }
                    catch { }
                }
                if (roles.ToList().IndexOf(roles.Max()) == 0)
                {
                    returnstring += "\n**Most Played Role: Top**";
                }
                else if (roles.ToList().IndexOf(roles.Max()) == 1)
                {
                    returnstring += "\n**Most Played Role: Jungle**";
                }
                else if (roles.ToList().IndexOf(roles.Max()) == 2)
                {
                    returnstring += "\n**Most Played Role: Mid**";
                }
                else if (roles.ToList().IndexOf(roles.Max()) == 3)
                {
                    returnstring += "\n**Most Played Role: ADC**";
                }
                else if (roles.ToList().IndexOf(roles.Max()) == 4)
                {
                    returnstring += "\n**Most Played Role: Support**";
                }

                champs.Sort();
                int x = 5;
                if (x >= champs.Count)
                {
                    x = champs.Count;
                }
                returnstring += "\n**Most Played Champions:**";
                for (int i = 0; i < x; i++)
                {
                    returnstring += "\n" + champs[i].Name + ": " + champs[i].Amount.ToString() + " Games";
                }
            }
            catch { }
            if (summoner.Level < 30)
            {
                returnstring += "\n**Account level: " + summoner.Level.ToString() + "**";
            }
            returnstring += "\n**Top Champion Mastery**:";
            var championmastery = api.GetTopChampionsMasteries(IntToPlatform(region), RiotID);
            int champions = 3;
            if (championmastery.Count < 3)
            {
                champions = championmastery.Count;
            }
            for (int i = 0; i < champions ; i++)
            {
                returnstring += "\n" + staticApi.GetChampion(r, (Convert.ToInt32((championmastery[i].ChampionId)))).Name + ": Level " + championmastery[i].ChampionLevel.ToString() +", "+ championmastery[i].ChampionPoints.ToString() + " Points";
            }
            return returnstring;
        }

        private string GetRankSimple(string summonername, Region r)
        {
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(r, summonername);
            string returnstring = "";
            string rank;
            string division;
            var stats = summoner.GetLeagues();
            foreach (var item in stats)
            {
                if (item.Queue == Queue.RankedSolo5x5)
                {
                    rank = item.Tier.ToString();
                    division =
                        item.Entries.Where(y => y.PlayerOrTeamId == summoner.Id.ToString())
                            .Select(y => y.Division)
                            .Single();
                    string lp =
                        item.Entries.Where(y => y.PlayerOrTeamId == summoner.Id.ToString())
                            .Select(y => y.LeaguePoints)
                            .Single()
                            .ToString();
                    returnstring += rank + " " + division + ": " + lp + " LP";
                }
            }
            return returnstring;
        }
        

        public string GetRole(int RiotID, Region r)
        {
            string returnstring = "";
            int[] roles = new int[5] { 0, 0, 0, 0, 0 };
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            DateTime starts7 = new DateTime(2016, 12, 7);
            var matches = api.GetMatchList(r, RiotID, null, null, null, starts7).Matches;
            foreach (var match in matches)
            {
                try
                {
                    if (match.Queue == Queue.RankedSolo5x5 || match.Season.ToString().Contains("Pre"))
                    {
                        if (match.Lane == Lane.Top)
                        {
                            roles[0] += 1;
                        }
                        else if (match.Lane == Lane.Jungle)
                        {
                            roles[1] += 1;
                        }
                        else if (match.Lane == Lane.Mid || match.Lane == Lane.Middle)
                        {
                            roles[2] += 1;
                        }
                        else if (match.Lane == Lane.Bot || match.Lane == Lane.Bottom && match.Role == Role.DuoCarry)
                        {
                            roles[3] += 1;
                        }
                        else if (match.Lane == Lane.Bot || match.Lane == Lane.Bottom && match.Role == Role.DuoSupport)
                        {
                            roles[4] += 1;
                        }
                    }
                }
                catch { }
            }
            if (roles.ToList().IndexOf(roles.Max()) == 0)
            {
                returnstring = "Top";
            }
            else if (roles.ToList().IndexOf(roles.Max()) == 1)
            {
                returnstring = "Jungle";
            }
            else if (roles.ToList().IndexOf(roles.Max()) == 2)
            {
                returnstring = "Mid";
            }
            else if (roles.ToList().IndexOf(roles.Max()) == 3)
            {
                returnstring = "ADC";
            }
            else if (roles.ToList().IndexOf(roles.Max()) == 4)
            {
                returnstring = "Support";
            }
            return returnstring;
        }
        public string ServerStatus(Region r)
        {
            string returnstring = "";
            var statusApi = StatusRiotApi.GetInstance();
            var shardStatus = statusApi.GetShardStatus(r);
            returnstring += "Status for region " + r.ToString()  + "\n";
            foreach (var service in shardStatus.Services)
            {
                returnstring += service.Name + ": " + service.Status + "\n";
                //returnstring += (service.Name);
                //foreach (var incident in service.Incidents)
                //{
                //    incident.Updates.ForEach(u => returnstring += ("  " + u.Content));
                //}
            }
            return returnstring;
        }
        public string GetCurrentGame(Region r, string summonername)
        {
            var staticApi = StaticRiotApi.GetInstance(APIKeys.riotkey);
            var api = RiotApi.GetInstance(APIKeys.riotkey);
            var summoner = api.GetSummoner(r, summonername);
            var game = api.GetCurrentGame(Platform.EUW1, summoner.Id);
            string[,] players = new string[3, game.Participants.Count];
            int[,] championids = new int[3, game.Participants.Count];
            int[,] bannedchampions = new int[2, 6];
            if (game.GameQueueType == GameQueueType.Normal5x5Draft || game.GameQueueType == GameQueueType.RankedSolo5x5 ||
                game.GameQueueType == GameQueueType.RankedPremade5x5 || game.GameQueueType == GameQueueType.TeamBuilderRankedSolo)
            {
                for (int i = 0; i < 6; i++)
                {
                    bannedchampions[game.BannedChampions[i].TeamId / 100 - 1, game.BannedChampions[i].PickTurn - 1] =
                        Convert.ToInt32(game.BannedChampions[i].ChampionId);
                }
            }
            for (int i = 0; i < game.Participants.Count; i++)
            {
                players[game.Participants[i].TeamId / 100 - 1, i] = game.Participants[i].SummonerName;
                championids[game.Participants[i].TeamId / 100 - 1, i] = Convert.ToInt32(game.Participants[i].ChampionId);
            }
            string returnstring = "";
            returnstring += "Team " + 1.ToString() + ": \n";
            returnstring += "Bans: " + staticApi.GetChampion(r, bannedchampions[0, 0]).Name + ", " +
                            staticApi.GetChampion(r, bannedchampions[0, 2]).Name + ", " +
                            staticApi.GetChampion(r, bannedchampions[0, 4]).Name + ".\n```";
            string player1 = staticApi.GetChampion(r,championids[0,0]).Name;
            string player2 =  staticApi.GetChampion(r, championids[0, 1]).Name;
            string player3 =  staticApi.GetChampion(r, championids[0, 2]).Name;
            string player4 =   staticApi.GetChampion(r, championids[0, 3]).Name;
            string player5 =  staticApi.GetChampion(r, championids[0, 4]).Name;

            string rank1 = "unranked";
            string rank2 = "unranked";
            string rank3 = "unranked";
            string rank4 = "unranked";
            string rank5 = "unranked";

            try
            {
                rank1 = GetRankSimple(players[0, 0], r);
            }
            catch
            {

            }
            try
            {
                rank2 = GetRankSimple(players[0, 1], r);
            }
            catch
            {

            }
            try
            {
                rank3 = GetRankSimple(players[0, 2], r);
            }
            catch
            {

            }
            try
            {
                rank4 = GetRankSimple(players[0, 3], r);
            }
            catch
            {

            }
            try
            {
                rank5 = GetRankSimple(players[0, 4], r);
            }
            catch
            {

            }
            string format1 = "{0,0} {1," + (25 - players[0, 0].Length).ToString() + "} " + "{2," +(20 + player1.Length - players[0, 0].Length).ToString()+ "}";
            string format2 = "{0,0} {1," + (25 - players[0, 1].Length).ToString() + "} " + "{2," + (20 + player2.Length - players[0, 1].Length).ToString() + "}";
            string format3 = "{0,0} {1," + (25 - players[0, 2].Length).ToString() + "} " + "{2," + (20 + player3.Length - players[0, 2].Length).ToString() + "}";
            string format4 = "{0,0} {1," + (25 - players[0, 3].Length).ToString() + "} " +"{2," + (20 + player4.Length - players[0, 3].Length).ToString() + "}";
            string format5 = "{0,0} {1," + (25 - players[0, 4].Length).ToString() + "} " + "{2," + (20 - player5.Length - players[0, 4].Length).ToString() + "}";

            string player1total = string.Format(format1,players[0,0] + ": ", player1,rank1);
            string player2total = string.Format(format2,players[0, 1] + ": ", player2,rank2);
            string player3total = string.Format(format3, players[0, 2] + ": ", player3, rank3);
            string player4total = string.Format(format4, players[0, 3] + ": ", player4, rank4);
            string player5total = string.Format(format5, players[0, 4] + ": ", player5, rank5);
            returnstring += "\n" + player1total + "\n" + player2total + "\n" + player3total + "\n" + player4total + "\n" +
                            player5total + "```";


            return returnstring;
        }
        public Region StringToRegion(string regionstring)
        {
            if (regionstring.ToLower() == "euw")
            {
                return Region.euw;
            }
            else if (regionstring.ToLower() == "na")
            {
                return Region.na;
            }
            else if (regionstring.ToLower() == "eune")
            {
                return Region.eune;
            }
            else if (regionstring.ToLower() == "oce")
            {
                return Region.oce;
            }
            else if (regionstring.ToLower() == "lan")
            {
                return Region.lan;
            }
            else if (regionstring.ToLower() == "las")
            {
                return Region.las;
            }
            else if (regionstring.ToLower() == "br")
            {
                return Region.br;
            }
            return Region.euw;
        }
        public Region IntToRegion(int id)
        {
            if (id == 1)
            {
                return Region.euw;
            }
            else if (id == 2)
            {
                return Region.na;
            }
            else if (id == 3)
            {
                return Region.eune;
            }
            else if (id == 4)
            {
                return Region.oce;
            }
            else if (id == 5)
            {
                return Region.lan;
            }
            else if (id == 6)
            {
                return Region.las;
            }
            else if (id == 7)
            {
                return Region.br;
            }
            return Region.euw;
        }
        public Platform IntToPlatform(int id)
        {
            if (id == 1)
            {
                return Platform.EUW1;
            }
            else if (id == 2)
            {
                return Platform.NA1;
            }
            else if (id == 3)
            {
                return Platform.EUN1;
            }
            else if (id == 4)
            {
                return Platform.OC1;
            }
            else if (id == 5)
            {
                return Platform.LA1;
            }
            else if (id == 6)
            {
                return Platform.LA2;
            }
            else if (id == 7)
            {
                return Platform.BR1;
            }
            return Platform.EUW1;
        }

        public int StringToRegionInt(string region)
        {
            if (region.ToLower() == "euw")
            {
                return 1;
            }
            else if (region.ToLower() == "na")
            {
                return 2;
            }
            else if (region.ToLower() == "eune")
            {
                return 3;
            }
            else if (region.ToLower() == "oce")
            {
                return 4;
            }
            else if (region.ToLower() == "las")
            {
                return 5;
            }
            else if (region.ToLower() == "lan")
            {
                return 6;
            }
            else if (region.ToLower() == "br")
            {
                return 7;
            }
            return 1;
        }
    }
    
}
