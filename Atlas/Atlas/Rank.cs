﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas
{
    public enum Rank
    {
        unranked,
        bronze,
        silver,
        gold,
        platinum,
        platinumIII,
        platinumII,
        platniumI,
        diamondV,
        diamondIV,
        diamondIII,
        diamondII,
        diamondI,
        master,
        challenger
    }
    public static class Diamond
    {
        public static string Converter(Rank diamond)
        {
            if (diamond == Rank.diamondV)
            {
                return "diamond 5";
            }
            else if (diamond == Rank.diamondIV)
            {
                return "diamond 4";
            }
            else if (diamond == Rank.diamondIII)
            {
                return "diamnd 3";
            }
            else if (diamond == Rank.diamondII)
            {
                return "diamond 2";
            }
            else if (diamond == Rank.diamondI)
            {
                return "diamond 1";
            }
            return "unranked";
        }
    }
}
