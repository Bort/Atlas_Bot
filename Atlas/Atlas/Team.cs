﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiotSharp;
namespace Atlas
{
    class Team
    {
        ulong ownerID;
        int teamSize;
        List<ulong> playersID = new List<ulong>();
        Region region;
        Rank minrank;
        Rank maxrank;
        Rank ownerRank;

        public List<ulong> PlayersID
        {
            get
            {
                return playersID;
            }

            set
            {
                playersID = value;
            }
        }

        public Team(ulong ownerID, int teamSize, Rank ownerRank, Region region)
        {
            playersID.Add(ownerID);
            this.ownerID = ownerID;
            this.teamSize = teamSize;
            this.region = region;
            if (ownerRank == Rank.bronze)
            {
                minrank = Rank.bronze;
                maxrank = Rank.silver;
            }
            else if (ownerRank == Rank.silver)
            {
                minrank = Rank.bronze;
                maxrank = Rank.gold;
            }
            else if (ownerRank == Rank.gold)
            {
                minrank = Rank.silver;
                maxrank = Rank.platinum;
            }
            else if (ownerRank == Rank.platinum)
            {
                minrank = Rank.gold;
                maxrank = Rank.diamondV;
            }
            else if (ownerRank == Rank.diamondV)
            {
                minrank = Rank.platinum; //plat 1
                maxrank = Rank.diamondIII;
            }
            else if (ownerRank == Rank.diamondIV)
            {
                minrank = Rank.platinum; //plat 1
                maxrank = Rank.diamondI;
            }
            else if (ownerRank == Rank.diamondIII)
            {
                minrank = Rank.platinum; //plat 1
                maxrank = Rank.master;
            }
            else if (ownerRank == Rank.diamondII)
            {
                minrank = Rank.diamondV;
                maxrank = Rank.master;
            }
            else if (ownerRank == Rank.diamondI)
            {
                minrank = Rank.diamondV;
                maxrank = Rank.master;
            }
            this.ownerRank = ownerRank;
        }
        public bool CheckTeam(Rank r)
        {
            if (r == minrank || r == ownerRank || r == maxrank && PlayersID.Count != teamSize)
            {
                return true;
            }
            return false;
        }
        public void AdjustMinMax(Rank r)
        {
            if (r == minrank)
            {
                maxrank = ownerRank;
            }
            else if (r == maxrank)
            {
                minrank = ownerRank;
            }
        }    
    }
}
