﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas
{
    class Champ : IComparable
    {
        private string championname;
        private int amount;
        private int championid;
        public Champ(string championname, int id)
        {
            this.championname = championname;
            this.championid = id;
            amount = 1;
        }
        public int Id
        {
            get { return championid; }
        }
        public void IncreaseAmount()
        {
            amount++;
        }
        public int CompareTo(object c)
        {
            if ((c as Champ).amount > this.amount)
            {
                return 1;
            }
            else if ((c as Champ).amount == this.amount)
            {
                return 0;
            }
            else if ((c as Champ).amount < this.amount)
            {
                return -1;
            }
            else return 0;
        }

        public int Amount
        {
            get { return amount; }
        }
        public string Name
        {
            get { return championname; }
        }
    }
}
