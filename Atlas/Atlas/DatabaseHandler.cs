﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Atlas
{
    class DatabaseHandler
    {
        SqlConnection Connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\bartd\Source\Repos\Atlas_Bot\Atlas\Atlas\Database1.mdf;Integrated Security=True");
        public void ClaimRiotAccount(string DiscordID, int RiotID, string Region)
        {
            int regionid = 1;
            if (Region == "euw")
            {
                regionid = 1;
            }
            else if (Region == "na")
            {
                regionid = 2;
            }
            else if (Region == "eune")
            {
                regionid = 3;
            }
            else if (Region == "oce")
            {
                regionid = 4;
            }
            else if (Region == "lan")
            {
                regionid = 5;
            }
            else if (Region == "las")
            {
                regionid = 6;
            }
            else if (Region == "br")
            {
                regionid = 7;
            }
            string query = "IF NOT EXISTS(SELECT 1 FROM [User] WHERE SummonerID = @RiotID OR DiscordID = @DiscordID) INSERT INTO [User] VALUES (@DiscordID, @RiotID, @RegionID)";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            cmd.Parameters.AddWithValue("@RiotID", RiotID);
            cmd.Parameters.AddWithValue("@RegionID", regionid);
            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
        public void RemoveRiotAccount(string DiscordID)
        {
            string query = "DELETE FROM [User] WHERE DiscordID = @DiscordID";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
        public int DiscordToRiot(string DiscordID)
        {
            string query = "SELECT SummonerID FROM [User] WHERE DiscordID = @DiscordID";
            int riotid = 0;
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            Connection.Open();
            using(SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    riotid = reader.GetInt32(0);
                }
            }
            Connection.Close();
            return riotid;
        }
        public void QueueUp(string DiscordID, int queue)
        {
            string query = "INSERT INTO [Queue] VALUES((SELECT [User].Id FROM [User] WHERE DiscordID = @DiscordID), @queue, (SELECT [User].RegionID FROM [User] WHERE DiscordID = @DiscordID))";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            cmd.Parameters.AddWithValue("@queue", queue);
            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
        public void LeaveQueue(string DiscordID)
        {
            string query = "DELETE [Queue] FROM [Queue] INNER JOIN [User] ON [User].Id = UserID WHERE [User].DiscordID = @DiscordID";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
        public int StringToQueueID(string Queue)
        {
            int returnint = 1;
            string query = "SELECT Id, Name FROM [TypeQ]";
            SqlCommand cmd = new SqlCommand(query, Connection);
            Connection.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (Queue.ToLower() == reader.GetString(1).ToLower())
                    {
                        returnint = reader.GetInt32(0);
                    }
                }
            }
            Connection.Close();
            return returnint;
        }
        public List<string> GetQueue(string region, int queueID)
        {
            string query = "SELECT [User].DiscordId FROM [Queue] INNER JOIN [User] ON [User].Id = UserID WHERE [Queue].RegionID = @Region AND TypeQID = @queueID";
            List<string> riotid = new List<string>();
            int regionid = 1;
            if (region == "euw")
            {
                regionid = 1;
            }
            else if (region == "na")
            {
                regionid = 2;
            }
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@Region", regionid);
            cmd.Parameters.AddWithValue("@queueID", queueID);
            Connection.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    riotid.Add(reader.GetString(0));
                }
            }
            Connection.Close();
            return riotid;
        }
        public void BanUser(string discordid, string discordname, string reason, string admin)
        {
            string query = "INSERT INTO [Bans] VALUES(@DiscordID, @DiscordName, @Reason, @AdminName)";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", discordid);
            cmd.Parameters.AddWithValue("@DiscordName", discordname);
            cmd.Parameters.AddWithValue("@Reason", reason);
            cmd.Parameters.AddWithValue("@AdminName", admin);
            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
        public string BannedUsers()
        {
            string returnstring = "Following Users are banned: ";
            string query = "SELECT Name, Reason, AdminName FROM [Bans]";
            SqlCommand cmd = new SqlCommand(query, Connection);
            Connection.Open();
            using(SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    returnstring += "\n" + reader.GetString(0) + " by " + reader.GetString(2) + " for " + reader.GetString(1);
                }
            }
            Connection.Close();
            return returnstring;
        }
        public int GetRegionDiscord(string discordid)
        {
            int region = 1;
            string query = "SELECT RegionID FROM [User] WHERE DiscordID = @DiscordID";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", discordid);
            Connection.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    region = reader.GetInt32(0);
                }
            }
            Connection.Close();
            return region;
        }
        public void CreateTeam(string TeamName, string OwnerID)
        {
            string query = "IF NOT EXISTS(SELECT 1 FROM [Team] WHERE Name = @Name OR OwnerID = @Owner) INSERT INTO [Team] VALUES (@Name, @Owner)";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@Name", TeamName);
            cmd.Parameters.AddWithValue("@Owner", OwnerID);
            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
        public bool AdminCheck(string DiscordID)
        {
            bool returnbool = false;
            string query = "SELECT U.DiscordID FROM [User] AS U INNER JOIN Admin ON Admin.UserID = U.Id";
            SqlCommand cmd = new SqlCommand(query, Connection);
            Connection.Open();
            using(SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader.GetString(0) == DiscordID)
                    {
                        returnbool = true;
                    }
                }
            }
            Connection.Close();
            return returnbool;
        }
        public int CountUsers()
        {
            int returnint = 0;
            string query = "SELECT COUNT(*) FROM [User]";
            SqlCommand cmd = new SqlCommand(query, Connection);
            Connection.Open();
            using(SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    returnint = reader.GetInt32(0);
                }
            }
            Connection.Close();
            return returnint;
        }
        public bool InviteToTeam(string DiscordID, int DiscordIDInvited)
        {
            try
            {
                string query = "INSERT INTO [Team_User] VALUES(@DiscordIDINV, (SELECT [Team].Id FROM [Team] WHERE OwnerID = @DiscordID), 1)";
                SqlCommand cmd = new SqlCommand(query, Connection);
                cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
                cmd.Parameters.AddWithValue("@DiscordIDINV", DiscordIDInvited);
                Connection.Open();
                cmd.ExecuteNonQuery();
                Connection.Close();
                return true;
            }
            catch
            {
                return false;
            }
            
        }
        public int GetIDInvite(string DiscordID, string TeamName)
        {
            int returnint = 0;
            string query = "SELECT [Team_User].Id FROM [Team_User] INNER JOIN [User] ON [User].Id = UserID INNER JOIN [Team] ON [Team].Id = TeamID WHERE [Team].Name = @TeamName AND [User].DiscordID = @DiscordID";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            cmd.Parameters.AddWithValue("@TeamName", TeamName);
            Connection.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    returnint = reader.GetInt32(0);
                }
            }
            Connection.Close();
            return returnint;
        }
        public void AcceptInvite(string DiscordID, int TeamID)
        {
            string query = "UPDATE [Team_User]  SET Invite = 0 FROM [Team_User] INNER JOIN [User] ON [User].ID = UserID WHERE TeamID = @TeamID AND [User].DiscordID = @DiscordID";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@TeamID", TeamID);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
        public int DiscordIDtoUserID(string DiscordID)
        {
            int returnint = 0;
            string query = "SELECT Id FROM [User] WHERE DiscordID = @DiscordID";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            Connection.Open();
            using(SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    returnint = reader.GetInt32(0);
                }
            }
            Connection.Close();
            return returnint;
        }
        public int OwnerToTeamID(string DiscordID)
        {
            int returnint = 0;
            string query = "SELECT [Team].Id FROM [Team] WHERE OwnerID = @DiscordID";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@DiscordID", DiscordID);
            Connection.Open();
            using(SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    returnint = reader.GetInt32(0);
                }
            }
            Connection.Close();
            return returnint;
        }
        public string IDToTeamName(int ID)
        {
            string returnstring = "error";
            string query = "SELECT Name FROM [Team] WHERE Id = @ID";
            SqlCommand cmd = new SqlCommand(query, Connection);
            cmd.Parameters.AddWithValue("@ID", ID);
            Connection.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    returnstring = reader.GetString(0);
                }
            }
            Connection.Close();
            return returnstring;
        }
    }
}
