﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas
{
    static class SpellingChecker
    {
        public static string Rolechecker(string role)
        {
            if (role.ToLower().Contains("top"))
            {
                return "top";
            }
            else if (role.ToLower().Contains("mid"))
            {
                return "mid";
            }
            else if (role.ToLower().Contains("junge"))
            {
                return "jungle";
            }
            else if (role.ToLower().Contains("ADC") || role.ToLower().Contains("bot") || role.ToLower().Contains("marksman"))
            {
                return "adc";
            }
            else if (role.ToLower().Contains("supp"))
            {
                return "support";
            }
            return "error";
        }
        
    }
}
