﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiotSharp;

namespace Atlas
{
    class Duo
    {
        ulong discordid;
        ulong invitedID;
        Rank rank;
        Region region;
        bool invitesent = false;
        bool accepted = false;
        bool inviteaccepted = false;
        List<ulong> rejectedids = new List<ulong>();
        public Duo(ulong discordid, Rank rank, Region region)
        {
            this.discordid = discordid;
            this.rank = rank;
            this.region = region;
        }
        public ulong InvitedID
        {
            get { return invitedID; }
            set { invitedID = value; }
        }
        public bool InviteAccepted
        {
            get { return inviteaccepted; }
            set { inviteaccepted = value; }
        }
        public bool Invite
        {
            get { return invitesent; }
            set { invitesent = value; }
        }
        public bool Accepted
        {
            set { accepted = value; }
            get { return accepted; }
        }
        public Rank GetRank
        {
            get { return rank; }
        }
        public Region GetRegion
        {
            get { return region; }
        }
        public ulong DiscordID
        {
            get { return discordid; }
        }
        public void RejectUser(ulong id)
        {
            rejectedids.Add(id);
        }
        public bool CheckCompatability(Rank r, Region reg, ulong id)
        {
           if (rejectedids.Contains(id) == false)
            {
                if ((rank == Rank.bronze || rank == Rank.silver || rank == Rank.gold) && region == reg)
                {
                    if (r == rank || r == Minrank(rank) || r == Maxrank(rank))
                    {
                        return true;
                    }
                }
                if (rank == Rank.platinum && (r == Rank.gold || r == Rank.platinumIII || r == Rank.platinumII || r == Rank.platniumI || r == Rank.platinum) && reg == region)
                {
                    return true;
                }
                else if (rank == Rank.platinumIII && reg == region && (r == Rank.gold || r == Rank.platinum || r == Rank.platinumIII || r == Rank.platinumII || r == Rank.platniumI || r == Rank.diamondV))
                {
                    return true;
                }
                else if (rank == Rank.platinumII && reg == region && (r == Rank.gold || r == Rank.platinumIII || r == Rank.platinumII || r == Rank.platniumI || r == Rank.platinum || r == Rank.diamondV || r == Rank.diamondIV))
                {
                    return true;
                }
                else if (rank == Rank.platniumI && reg == region && (r == Rank.gold || r == Rank.platinumIII || r == Rank.platinumII || r == Rank.platniumI || r == Rank.platinum || r == Rank.diamondV || r == Rank.diamondIV || r == Rank.diamondIII))
                {
                    return true;
                }
                else if (rank == Rank.diamondV && reg == region && (r == Rank.platinumIII || r == Rank.platinumII || r == Rank.platniumI || r == Rank.diamondV || r == Rank.diamondIV || r == Rank.diamondIII || r == Rank.diamondII || r == Rank.diamondI))
                {
                    return true;
                }
                else if (rank == Rank.diamondIV && reg == region && (r == Rank.platinumII || r == Rank.platniumI || r == Rank.diamondV || r == Rank.diamondIV || r == Rank.diamondIII || r == Rank.diamondII || r == Rank.diamondI))
                {
                    return true;
                }
                else if (rank == Rank.diamondIII && reg == region && (r == Rank.platniumI || r == Rank.diamondV || r == Rank.diamondIV || r == Rank.diamondIII || r == Rank.diamondII || r == Rank.diamondI || r == Rank.master))
                {
                    return true;
                }
                //CHECK THIS DATA WHEN RIOT SUPPORT IS BACK UP
                else if ((rank == Rank.diamondII || rank == Rank.diamondI) && reg == region && (r == Rank.diamondV || r == Rank.diamondIV || r == Rank.diamondIII || r == Rank.diamondII || r == Rank.diamondI || r == Rank.master))
                {
                    return true;
                }
                else if (rank == Rank.master && reg == region && (r == Rank.diamondIII || r == Rank.diamondII || r == Rank.diamondI || r == Rank.master))
                {
                    return true;
                }
            }
            return false;
        }
        public Rank Minrank(Rank r)
        {
            if (r == Rank.bronze || r == Rank.silver)
            {
                return Rank.bronze;
            }
            else if (r == Rank.gold)
            {
                return Rank.silver;
            }
            return Rank.bronze;
        }
        public Rank Maxrank(Rank r)
        {
            if (r == Rank.bronze)
            {
                return Rank.silver;
            }
            else if (r == Rank.silver)
            {
                return Rank.gold;
            }
            else if (r == Rank.gold)
            {
                return Rank.platinum;
            }
            return Rank.master;
        }
    }
}
